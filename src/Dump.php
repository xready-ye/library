<?php
//TODO: Do something with static in can be registry - use path or another way

namespace xr\library;

use xr\library\dump\Show;
use xr\library\dump\Variable;

class Dump {
    static public function dump($var, ?string $name = null, ?int $depth = null) {
        $dump = new self();
        $dump->get($var, $name, $depth);
    }

    private function __construct() {
    }

    private function get($var, ?string $name, ?int $depth) {
        $show = new Show($depth);

        echo $show->show([
            Variable::factory($name, $var)
        ]);
    }
}