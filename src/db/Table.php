<?php


namespace xr\library\db;


use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;
use yii\helpers\VarDumper;

class Table
{
    static private $tables = [];

    /**
     * @return static
     */

    static public function factory(string $tableName, Connection $db = null) {
        if (!array_key_exists($tableName, static::$tables)) {
            $appClassName = 'app\\tables\\'.$tableName.'Table';

            if (class_exists($appClassName, true)) {
                $table = new $appClassName($tableName, $db);

                Instance::ensure($table, self::class);

                static::$tables[$tableName] = $table;
            } else {
                static::$tables[$tableName] = new static($tableName, $db);
            }
        }

        return static::$tables[$tableName];
    }

    public $db;

    public string $name;
    public array $columns = [];
    public array $columnsNames = [];
    public ?string $primaryKey = null;
    public array $primaryKeys = [];
    public array $foreignKeys = [];

    protected function __construct(string $tableName, Connection $db = null)
    {
        if ($db === null) {
            $db = \Yii::$app->getDb();
        }

        $this->db = $db;

        $info = $this->db->getTableSchema($tableName);

        $this->name = &$info->name;

        //Get columns
        $this->columns = &$info->columns;
        $this->columnsNames = array_keys($this->columns);

        //Get primary keys
        foreach ($this->columns as $column) {
            if ($column->isPrimaryKey) {
                $this->primaryKeys[] = $column->name;
            }
        }

        if (count($this->primaryKeys) === 1) {
            $this->primaryKey = $this->primaryKeys[0];
        }

        //Get foreign keys
        foreach ($info->foreignKeys as $foreignKey) {
            foreach ($foreignKey as $foreignKeyIndex => $foreignKeyValue) {
                if ($foreignKeyIndex === 0) {
                    $foreignKeyTable = $foreignKeyValue;
                } else {
                    $foreignKeyColumn = $foreignKeyIndex;
                    $foreignKeyForeignColumn = $foreignKeyValue;
                }
            }

            if (!array_key_exists($foreignKeyColumn, $this->foreignKeys)) {
                $this->foreignKeys[$foreignKeyColumn] = [
                    'column' => $foreignKeyColumn
                ];
            }

            $this->foreignKeys[$foreignKeyColumn]['foreignTable'] = $foreignKeyTable;
            $this->foreignKeys[$foreignKeyColumn]['foreignColumn'] = $foreignKeyForeignColumn;
        }

        //VarDumper::dump($this, 10, true); die;

        //VarDumper::dump($this->info, 10, true);
    }

    public function getTableName() {
        return $this->name;
    }

    public function select($fields, $where = [], $order = [], $group = [], $limit = null, $offset = null) {
        return new Select($this, $fields, $where, $order, $group, $limit, $offset);
    }

    public function get($fields, $where = [], $order = [], $group = [], $limit = null, $offset = null) {
        $select = $this->select($fields, $where, $order, $group, $limit, $offset);

        return $select->all();
    }
}