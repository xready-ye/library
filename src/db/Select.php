<?php


namespace xr\library\db;


use yii\db\Connection;
use yii\db\Query;

class Select extends Query
{
    private $table;

    private function parseFields($fields) {
        return $fields;
    }

    private function parseWhere($where) {
        return $where;
    }

    public function __construct($table, $fields, $where = [], $order = [], $group = [], $limit = null, $offset = null) {
        parent::__construct();

        if ($table instanceof Table) {
            $this->table = $table;
        } elseif (is_string($table)) {
            $this->table = Table::factory($table);
        } else {
            //TODO: Exception
        }

        $this->select($this->parseFields($fields));
        $this->from($this->table->getTableName());

        if (count($where) > 0) {
            $this->where($this->parseWhere($where));
        }

        if (count($order) > 0) {
            $this->orderBy($order);
        }

        if (count($group) > 0) {
            $this->groupBy($order);
        }

        if ($offset !== null) {
            $this->offset = $offset;
        }

        if ($limit !== null) {
            $this->limit = $limit;
        }
    }


}