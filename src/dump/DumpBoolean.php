<?php


namespace xr\library\dump;


class DumpBoolean extends Variable {
    protected function __construct($name, $var, $type, $path) {
        parent::__construct($name, $var, $type, $path);

        if ($var) {
            $this->value = 'true';
        } else {
            $this->value = 'false';
        }
    }
}