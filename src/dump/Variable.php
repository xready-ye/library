<?php
/**
 * Created by PhpStorm.
 * User: lagutin
 * Date: 24.11.15
 * Time: 22:07
 */

namespace xr\library\dump;


class Variable {
    static public function factory($name, $var, $path = []) : self {
        $type = gettype($var);

        switch ($type) {
            case 'integer':
            case 'double':
            case 'float':
                return new DumpScalar($name, $var, $type, $path);
            default:
                $className = 'xr\\library\\dump\\Dump'.ucfirst($type);

                if (class_exists($className)) {
                    return new $className($name, $var, $type, $path);
                }
                return new Variable($name, $var, $type, $path);
        }
    }

    protected
        $path;

    public
        $name,
        $type,
        $value,
        $children = array(),

        //Objects
        $class,
        $parents    = array(),
        $interfaces = array(),
        $double     = false,

        //Properties
        $visibility = array(),
        $owner,

        //Array
        $size,

        //String
        $full;

    protected function __construct($name, $var, $type, $path) {
        $this->name = $name;
        $this->type = $type;
        $this->path = $path;
    }
}