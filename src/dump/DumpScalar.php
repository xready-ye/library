<?php


namespace xr\library\dump;


class DumpScalar extends Variable {
    protected function __construct($name, $var, $type, $path) {
        parent::__construct($name, $var, $type, $path);

        $this->value = $var;
    }
}