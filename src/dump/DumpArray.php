<?php


namespace xr\library\dump;


class DumpArray extends Variable {
    protected function __construct($name, $var, $type, $path) {
        parent::__construct($name, $var, $type, $path);

        $this->size = count($var);

        if ($this->size > 0) {
            foreach ($var as $index => $item) {
                if (is_numeric($index)) {
                    $this->children[] = Variable::factory($index, $item, $this->path);
                } else {
                    $this->children[$index] = Variable::factory($index, $item, $this->path);
                }
            }
        }
    }
}