<?php


namespace xr\library\dump;


class Show {
    private
        $depth;

    public function __construct($depth) {
        $this->depth = $depth;
    }

    public function show(array $dump, string $id = '', int $level = 0) {
        $ret = '';

        $idAttr = '';
        $displayStyle = '';

        if ($id !== '') {
            $idAttr = 'id="' . $id . '"';
        }

        if ($this->depth !== null && $level > $this->depth) {
            $displayStyle = ' display: none;';
        }

        $ret .= "<ul style=\"font-family: monospace;$displayStyle\" $idAttr>\n";

        foreach ($dump as $item) {
            $header = array();

            //Show visibility
            if (count($item->visibility) > 0) {
                $header['visibility'] = '<span style="color: grey;">' . implode(' ', $item->visibility) . '</span>';
            }

            if (!is_null($item->name)) {
                $header['name'] = '<strong>' . $item->name . '</strong>';
            }

            if (!is_null($item->owner)) {
                if (!is_null($item->name)) {
                    $header['name'] .= '<span style="color: grey;">:' . $item->owner . '</span>';
                } else {
                    $header['owner'] = '<span style="color: grey;">' . $item->owner . '</span>';
                }

            }

            if (!is_null($item->name) && !is_null($item->type)) {
                $header['sep'] = '=>';
            }

            if (!is_null($item->type)) {
                $header['type'] = '<span style="color: green; font-weight: bold;">' . $item->type . '</span>';
            }

            if (!is_null($item->size)) {
                $header['size'] = '<span style="color: grey;">(' . $item->size . ')</span>';
            }

            if (!is_null($item->class)) {
                $header['class'] = '<span style="color: grey;">instance of <strong>' . $item->class . '</strong></span>';
            }

            if (count($item->parents) > 0) {
                foreach ($item->parents as $index => $parent) {
                    $header['parent'.$index] = '<span style="color: grey;">extends <strong>' . $parent . '</strong></span>';
                }
            }

            if (count($item->interfaces) > 0) {
                foreach ($item->interfaces as $index => $interface) {
                    $header['interface'.$index] = '<span style="color: grey;">implements <strong>' . $interface . '</strong></span>';
                }
            }

            if (!is_null($item->value)) {
                $header['value'] = htmlspecialchars($item->value);
            }

            if ($item->double) {
                $header['double'] = '<span style="color: red;">...</span>';
            }

            $children = '';

            if (count($item->children) > 0 || !is_null($item->full)) {
                $childrenId = uniqid('', false);
                $header['expander'] = "<a href=\"#\" onclick=\"document.getElementById('$childrenId').style.display = ((document.getElementById('$childrenId').style.display === ''))?'none':''; return false;\">&plusmn;</a>";

                if (count($item->children) > 0) {
                    $children = $this->show($item->children, $childrenId, $level+1);
                } elseif (!is_null($item->full)) {
                    $children = '<pre id="'.$childrenId.'" style="display: none;">'.htmlspecialchars($item->full).'</pre>';
                }
            }

            $ret .= '<li>' . implode(' ', $header) . "\n";

            $ret .= "$children\n";
        }

        $ret .= "</ul>\n";

        return $ret;
    }
}