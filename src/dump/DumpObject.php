<?php
/**
 * Created by PhpStorm.
 * User: lagutin
 * Date: 24.11.15
 * Time: 22:17
 */

namespace xr\library\dump;


class DumpObject extends Variable {
    static
        array $registry = [];

    private $object;

    private function dumpProperty(\ReflectionProperty $reflectionProperty) {
        $reflectionProperty->setAccessible(true);

        if (false && $reflectionProperty->isStatic()) {
            $property = Variable::factory($reflectionProperty->name, null, $this->path);
            $property->visibility[] = 'static';
            $property->double = true;
        } else {
            $property = Variable::factory($reflectionProperty->name, $reflectionProperty->getValue($this->object), array_merge($this->path, [$reflectionProperty->class]));

            $property->owner = $reflectionProperty->class;

            if ($reflectionProperty->isStatic()) {
                $property->visibility[] = 'static';
            }

            if ($reflectionProperty->isPrivate()) {
                $property->visibility[] = 'private';
            }

            if ($reflectionProperty->isProtected()) {
                $property->visibility[] = 'protected';
            }

            if ($reflectionProperty->isPublic()) {
                $property->visibility[] = 'public';
            }
        }

        return $property;
    }

    private function dumpProperties(\ReflectionClass $reflection, $properties = array()) {
        $reflectionProperties = $reflection->getProperties();

        foreach ($reflectionProperties as $reflectionProperty) {
            if (!array_key_exists($reflectionProperty->name, $properties)) {
                $properties[$reflectionProperty->name] = $this->dumpProperty($reflectionProperty);
            }
        }

        $parent = $reflection->getParentClass();

        if ($parent) {
            $properties = $this->dumpProperties($parent, $properties);
        }

        return $properties;
    }

    private function dumpParents(\ReflectionClass $reflection, $parents = array()) {
        $parent = $reflection->getParentClass();

        if ($parent) {
            $parents[] = $parent->getName();

            $parents = $this->dumpParents($parent, $parents);
        }

        return $parents;
    }

    protected function __construct($name, $var, $type, $path) {
        parent::__construct($name, $var, $type, $path);

        $this->object = $var;

        $reflection = new \ReflectionObject($this->object);

        $this->class      = $reflection->getName();
        $this->interfaces = $reflection->getInterfaceNames();
        $this->parents    = $this->dumpParents($reflection);

        $hash = spl_object_hash($this->object);

        //var_dump($hash); die;

        if (in_array($hash, self::$registry)) {
            $this->double = true;
        } elseif ($this->class === end($this->path)) {
            $this->double = true;
        } else {
            self::$registry[] = $hash;
            $this->path[] = $this->class;
            $this->children = $this->dumpProperties($reflection);
        }
    }
}