<?php


namespace xr\library\dump;


class DumpString extends Variable {
    public function __construct($name, $var, $type, $path) {
        parent::__construct($name, $var, $type, $path);

        if (strlen($var) > 32) {
            $this->value = "\"".substr($var, 0, 32)."...\"";
            $this->full = $var;
        } else {
            $this->value = "\"$var\"";
        }
    }
}