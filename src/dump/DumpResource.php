<?php


namespace xr\library\dump;


class DumpResource extends Variable {
    protected function __construct($name, $var, $type, $path) {
        parent::__construct($name, $var, $type, $path);

        $this->size = (int)$var;
        $this->value = get_resource_type($var);
    }
}