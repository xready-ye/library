<?php


namespace xr\library\dump;


class Table {


    private function getTableHeader($dump) {
        $header = array();

        foreach ($dump->children as $row) {
            foreach ($row->children as $item) {
                if ($row->type === 'array') {
                    if (!in_array($item->name, $header)) {
                        $header[] = $item->name;
                    }
                } elseif ($row->type === 'object') {
                    if (in_array('public', $item->visibility) && !in_array($item->name, $header)) {
                        $header[] = $item->name;
                    }
                }
            }
        }

        return $header;
    }

    private function getTableValue($row, $field) {
        if (array_key_exists($field, $row->children)) {
            $ret = array();

            if (property_exists($row->children[$field], 'type')) {
                $ret[] = '<span style="color: green; font-weight: bold;">' . $row->children[$field]->type . '</span>';
            }

            if (property_exists($row->children[$field], 'value')) {
                $ret[] = $row->children[$field]->value;
            }

            return implode('<br/>', $ret);
        }

        return '';
    }

    private function getTable($var) {
        $dump = Variable::factory(null, $var);

        if (
            $dump->type === 'array' &&
            count($dump->children > 0) &&
            array_key_exists(0, $dump->children) &&
            (
                $dump->children[0]->type === 'array' ||
                $dump->children[0]->type === 'object'
            )
        ) {
            $header = $this->getTableHeader($dump);

            $ret = '';

            $ret .= '<table>';

            $ret .= '<tr>';

            foreach ($header as $field) {
                $ret .= '<td style="text-align: center; padding: 5px; font-weight: bold;">'.$field.'</td>';
            }

            $ret .= '</tr>';

            foreach ($dump->children as $row) {
                $ret .= '<tr>';

                foreach ($header as $field) {
                    $ret .= '<td style="text-align: center; padding: 5px; border-top: 1px solid black;">'.$this->getTableValue($row, $field).'</td>';
                }

                $ret .= '</tr>';
            }

            $ret .= '</table>';
        }

        echo $ret;
    }
}