<?php


namespace xr\library;

use xr\library\db\Table;
use yii\base\BaseObject;
use yii\di\Instance;
use yii\db\Connection;

class Db extends BaseObject
{
    public $db = 'db';

    public function init()
    {
        $this->db = Instance::ensure($this->db, Connection::className());

        parent::init();
    }

    public function table($tableName) : Table {
        return Table::factory($tableName, $this->db);
    }
}